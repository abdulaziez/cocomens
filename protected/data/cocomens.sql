-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 08, 2015 at 07:40 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cocomens`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_barang`
--

CREATE TABLE IF NOT EXISTS `tb_barang` (
`id` bigint(20) NOT NULL,
  `idkategori` bigint(20) NOT NULL,
  `nama_barang` varchar(255) NOT NULL,
  `harga` int(100) NOT NULL,
  `deskripsi` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_foto`
--

CREATE TABLE IF NOT EXISTS `tb_foto` (
`id` int(10) NOT NULL,
  `idbarang` bigint(20) NOT NULL,
  `foto` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_kategori`
--

CREATE TABLE IF NOT EXISTS `tb_kategori` (
`id` bigint(20) NOT NULL,
  `kategori` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tb_kategori`
--

INSERT INTO `tb_kategori` (`id`, `kategori`) VALUES
(1, 'Baju Kaos'),
(2, 'Celana Pendek'),
(3, 'Celana Panjang');

-- --------------------------------------------------------

--
-- Table structure for table `tb_keranjang`
--

CREATE TABLE IF NOT EXISTS `tb_keranjang` (
  `ids` varchar(100) NOT NULL,
  `no_order` varchar(5) DEFAULT NULL,
  `idbarang` bigint(20) NOT NULL,
  `idukuran` bigint(20) NOT NULL,
  `jumlahpembelian` int(100) NOT NULL,
  `jumlahharga` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_op`
--

CREATE TABLE IF NOT EXISTS `tb_op` (
`id` int(2) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `level_id` int(2) NOT NULL,
  `has` varchar(50) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tb_op`
--

INSERT INTO `tb_op` (`id`, `username`, `password`, `level_id`, `has`) VALUES
(1, 'admin', '7bc6bc8d9b81b250e605cfedd4af96a3', 1, '55759710482d83.58246895');

-- --------------------------------------------------------

--
-- Table structure for table `tb_order`
--

CREATE TABLE IF NOT EXISTS `tb_order` (
`id` bigint(20) NOT NULL,
  `no_order` varchar(5) NOT NULL,
  `totalharga` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_penjualan`
--

CREATE TABLE IF NOT EXISTS `tb_penjualan` (
`id` bigint(20) NOT NULL,
  `iduser` bigint(20) NOT NULL,
  `no_order` varchar(5) NOT NULL,
  `biayakirim` int(100) NOT NULL,
  `total_harga` int(100) NOT NULL,
  `diskon` int(100) NOT NULL,
  `status_pembayaran` enum('cash','transfer','belumterbayar','lunas') NOT NULL,
  `status_pengiriman` enum('inden','gudang','dikirim','diterima') NOT NULL,
  `tgl_pesan` date NOT NULL,
  `jam_pesan` time NOT NULL,
  `tgl_pengiriman` date NOT NULL,
  `kode_pembelian` varchar(100) NOT NULL,
  `verifikasi_email` enum('sudah','belum') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_ukuran`
--

CREATE TABLE IF NOT EXISTS `tb_ukuran` (
`id` bigint(20) NOT NULL,
  `idbarang` bigint(20) NOT NULL,
  `ukuran` varchar(255) NOT NULL,
  `stok` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE IF NOT EXISTS `tb_user` (
`id` bigint(20) NOT NULL,
  `user` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `namadepan` varchar(255) NOT NULL,
  `namabelakang` varchar(255) DEFAULT NULL,
  `namaperusahaan` varchar(255) DEFAULT NULL,
  `alamat` text,
  `kota` varchar(255) DEFAULT NULL,
  `provensi` varchar(255) DEFAULT NULL,
  `kodepos` varchar(100) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `telephon` varchar(20) DEFAULT NULL,
  `level_id` int(10) NOT NULL,
  `kode` varchar(255) NOT NULL,
  `status` enum('belum','sudah') NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id`, `user`, `password`, `namadepan`, `namabelakang`, `namaperusahaan`, `alamat`, `kota`, `provensi`, `kodepos`, `email`, `telephon`, `level_id`, `kode`, `status`) VALUES
(1, 'admin', '7cc0a6a0ae77992382e09107e98f7406', 'Abdul', 'Azis', NULL, 'jl.petarani 4 no 63 ', NULL, NULL, NULL, 'abdulaziz@gmail.com', '03114678901', 4, '554339018bc525.05503631', 'belum'),
(2, 'seno', 'aa71be40421b81f37634eb0f642bd959', 'seno', 'apriliyadi', NULL, 'tamangaparaya tiga ', NULL, NULL, NULL, 'seno.apriliyadi@gmail.com', '0338923823', 1, '554339c6a960c4.05527595', 'belum'),
(3, 'seno', '9edb0059abb96bae762161c78cf13d5d', 'seno', 'apriliyadi', NULL, 'tamangaparaya tiga ', NULL, NULL, NULL, 'seno.apriliyadi@gmail.com', '0338923823', 4, '554339ebd42238.08961641', 'belum');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_barang`
--
ALTER TABLE `tb_barang`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_foto`
--
ALTER TABLE `tb_foto`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_kategori`
--
ALTER TABLE `tb_kategori`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_keranjang`
--
ALTER TABLE `tb_keranjang`
 ADD KEY `idbrgbeli` (`no_order`);

--
-- Indexes for table `tb_op`
--
ALTER TABLE `tb_op`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_order`
--
ALTER TABLE `tb_order`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_penjualan`
--
ALTER TABLE `tb_penjualan`
 ADD PRIMARY KEY (`id`), ADD KEY `iduser` (`iduser`), ADD KEY `idorder` (`no_order`);

--
-- Indexes for table `tb_ukuran`
--
ALTER TABLE `tb_ukuran`
 ADD PRIMARY KEY (`id`), ADD KEY `idbarang` (`idbarang`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_barang`
--
ALTER TABLE `tb_barang`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_foto`
--
ALTER TABLE `tb_foto`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_kategori`
--
ALTER TABLE `tb_kategori`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_op`
--
ALTER TABLE `tb_op`
MODIFY `id` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_order`
--
ALTER TABLE `tb_order`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_penjualan`
--
ALTER TABLE `tb_penjualan`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_ukuran`
--
ALTER TABLE `tb_ukuran`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
