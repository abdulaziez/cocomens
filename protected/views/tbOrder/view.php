<?php
$this->breadcrumbs=array(
	'Tb Orders'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List TbOrder','url'=>array('index')),
array('label'=>'Create TbOrder','url'=>array('create')),
array('label'=>'Update TbOrder','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete TbOrder','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage TbOrder','url'=>array('admin')),
);
?>

<h1>View TbOrder #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'totalharga',
),
)); ?>
