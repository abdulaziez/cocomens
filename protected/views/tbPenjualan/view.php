<?php
$this->breadcrumbs=array(
	'Tb Penjualans'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List TbPenjualan','url'=>array('index')),
array('label'=>'Create TbPenjualan','url'=>array('create')),
array('label'=>'Update TbPenjualan','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete TbPenjualan','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage TbPenjualan','url'=>array('admin')),
);
?>

<h1>View TbPenjualan #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'iduser',
		'idorder',
		'biayakirim',
		'total_harga',
		'diskon',
		'status_pembayaran',
		'status_pengiriman',
		'tgl_pesan',
		'jam_pesan',
		'tgl_pengiriman',
		'kode_pembelian',
		'verifikasi_email',
),
)); ?>
