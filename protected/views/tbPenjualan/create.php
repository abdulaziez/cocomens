<?php
$this->breadcrumbs=array(
	'Tb Penjualans'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List TbPenjualan','url'=>array('index')),
array('label'=>'Manage TbPenjualan','url'=>array('admin')),
);
?>

<h1>Create TbPenjualan</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>