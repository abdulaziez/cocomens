<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('iduser')); ?>:</b>
	<?php echo CHtml::encode($data->iduser); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idorder')); ?>:</b>
	<?php echo CHtml::encode($data->idorder); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('biayakirim')); ?>:</b>
	<?php echo CHtml::encode($data->biayakirim); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('total_harga')); ?>:</b>
	<?php echo CHtml::encode($data->total_harga); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('diskon')); ?>:</b>
	<?php echo CHtml::encode($data->diskon); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status_pembayaran')); ?>:</b>
	<?php echo CHtml::encode($data->status_pembayaran); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('status_pengiriman')); ?>:</b>
	<?php echo CHtml::encode($data->status_pengiriman); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tgl_pesan')); ?>:</b>
	<?php echo CHtml::encode($data->tgl_pesan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jam_pesan')); ?>:</b>
	<?php echo CHtml::encode($data->jam_pesan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tgl_pengiriman')); ?>:</b>
	<?php echo CHtml::encode($data->tgl_pengiriman); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kode_pembelian')); ?>:</b>
	<?php echo CHtml::encode($data->kode_pembelian); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('verifikasi_email')); ?>:</b>
	<?php echo CHtml::encode($data->verifikasi_email); ?>
	<br />

	*/ ?>

</div>