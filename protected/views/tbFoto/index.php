<?php
$this->breadcrumbs=array(
	'Tb Fotos',
);

$this->menu=array(
array('label'=>'Create TbFoto','url'=>array('create')),
array('label'=>'Manage TbFoto','url'=>array('admin')),
);
?>

<h1>Tb Fotos</h1>

<?php $this->widget('booster.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
