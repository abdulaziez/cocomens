<?php
$this->breadcrumbs=array(
	'Tb Fotos'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List TbFoto','url'=>array('index')),
array('label'=>'Create TbFoto','url'=>array('create')),
array('label'=>'Update TbFoto','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete TbFoto','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage TbFoto','url'=>array('admin')),
);
?>

<h1>View TbFoto #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'idbarang',
		'foto',
),
)); ?>
