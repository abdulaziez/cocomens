<?php
$this->breadcrumbs=array(
	'Tb Ukurans'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List TbUkuran','url'=>array('index')),
	array('label'=>'Create TbUkuran','url'=>array('create')),
	array('label'=>'View TbUkuran','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage TbUkuran','url'=>array('admin')),
	);
	?>

	<h1>Update TbUkuran <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>