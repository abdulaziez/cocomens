<?php
$this->breadcrumbs=array(
	'Tb Ukurans'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List TbUkuran','url'=>array('index')),
array('label'=>'Create TbUkuran','url'=>array('create')),
array('label'=>'Update TbUkuran','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete TbUkuran','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage TbUkuran','url'=>array('admin')),
);
?>

<h1>View TbUkuran #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'idbarang',
		'ukuran',
		'stok',
),
)); ?>

