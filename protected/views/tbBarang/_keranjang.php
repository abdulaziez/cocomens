<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'tb-keranjang-form',
	'enableAjaxValidation'=>true,
)); ?>

<h3>Order <small>Sekarang</small></h3>
<hr>
<?php echo $form->errorSummary($model_keranjang);

?>
             

<?php echo $form->dropDownListGroup(
			$model_keranjang,
			'idukuran',
			array(
				'wrapperHtmlOptions' => array(
					'class' => 'col-sm-5',
				),
				'widgetOptions' => array(
					'data' => CHtml::listData(TbUkuran::model()->findAllByAttributes(array('idbarang'=>$model->id)),'ukuran','ukuran'),
					'htmlOptions' => array(),
				)
			)
	); ?>

        <?php echo $form->dropDownListGroup(
			$model_keranjang,
			'jumlahpembelian',
			array(
				'wrapperHtmlOptions' => array(
					'class' => 'col-sm-5',
				),
				'widgetOptions' => array(
					'data' => array(),
					'htmlOptions' => array(),
				)
			)
		); ?>


                
<?php 
echo CHtml::dropDownList('country_id','', array(1=>'USA',2=>'France',3=>'Japan'),
array(
'ajax' => array(
'type'=>'POST', //request type
'url'=>CController::createUrl('TbBarangController/dynamiccities'), //url to call.
//Style: CController::createUrl('currentController/methodToCall')
'update'=>'#jumlahpembelian', //selector to update
//'data'=>'js:javascript statement' 
//leave out the data key to pass all form values through
))); 
 
//empty since it will be filled by the other dropdown
echo CHtml::dropDownList('jumlahpembelian','', array());

?>





<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model_keranjang->isNewRecord ? 'Order' : 'Save',
		)); ?>
</div>



<?php $this->endWidget(); ?>

