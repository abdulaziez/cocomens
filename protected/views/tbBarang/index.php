
<?php
$this->breadcrumbs=array(
	'TbBarang',
);
$dt = array ();
$dt[0] = array('label' =>'Categori','itemOptions' => array('calss' => 'nav-header'));
$Categori = TbKategori::model()->findAllByAttributes(array());
foreach ($Categori as $Hasil){
$dt[]=array('label'=>$Hasil['kategori'],'url'=>array('index','id'=>$Hasil['id']));
}

$this->menu=$dt;

?>

<h1>Tb Barangs</h1>


     <?php 
foreach ($tampung as $data):
	



     $dr = array();
     $foto = TbFoto::model()->findAllByAttributes(array('idbarang'=>$data->id));
     if(!$foto){
            $dr[0] = "noimage.jpg";
     }else{
     foreach ($foto as $d){
       
       $dr[] = $d['id'].'-'.$d['foto'];
     }}
   ?>

<div class="col-sm-5 col-md-3">
    <div class="thumbnail">
<?php $kategori = TbKategori::model()->findByPk($data->idkategori);?>
        <div class="caption">
			<span class="label label-danger"><?php echo CHtml::encode($kategori->kategori);?></span>
<?php	  		echo CHtml::link("<img src='images/produk/".$dr[0]."' alt='' width='235' height='335' id='".CHtml::encode($data->id)."' onmouseenter='a".CHtml::encode($data->id)."()' onmouseleave='b".CHtml::encode($data->id)."()'>",array('view','id'=>$data->id)); ?>
            <div class="carousel-caption" id=<?php echo CHtml::encode($data->id)."cr"?> >
				<h4><?php echo CHtml::encode($data->nama_barang); ?></h4>
				<p>Harga : <?php echo CHtml::encode($data->harga); ?> IDR</p>
			</div>
        </div>


        
    </div>
</div>
<script>
function a<?php echo CHtml::encode($data->id)?>() {
    <?php if(count($dr)>=2){ ?>
    document.getElementById("<?php echo CHtml::encode($data->id)?>").src = "images/produk/<?php echo $dr[1]?>";
    <?php } ?>
}

function b<?php echo CHtml::encode($data->id)?>() {
    document.getElementById("<?php echo CHtml::encode($data->id)?>").src = "images/produk/<?php echo $dr[0]?>";
}
</script>
<?php endforeach; ?>