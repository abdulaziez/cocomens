<?php
$this->breadcrumbs=array(
	'Tb Barangs'=>array('index'),
	$model->id,
);
/*
$this->menu=array(
array('label'=>'List TbFoto','url'=>array('index')),
array('label'=>'Create TbFoto','url'=>array('create')),
array('label'=>'Update TbFoto','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete TbFoto','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage TbFoto','url'=>array('admin')),
);*/
?>

<div class="row">
	<div class="col-md-3">

 <?php   
     $dr = array();
     $foto = TbFoto::model()->findAllByAttributes(array('idbarang'=>$model->id));
     foreach ($foto as $d){
       
       $dr[] = 'images/produk/'.$d['id'].'-'.$d['foto'];
     }
   ?>

<?php 
		$this->widget('adGallery.AdGallery',
				array(
					'agWidth'=>340,
					'agHeight'=> 510,
					'agThumbWidth' => 100,
					'imageList' => $dr,
				)
			);
?>

	</div>
	<div class="col-md-9">
            <div class="row"> 
                <div class="col-sm-offset-2 col-sm-8">
                    <h1><?php echo $model->nama_barang; ?></h1>
                    <hr>
                    <h3  class="alert alert-success text-right" ><?php echo $model->harga; ?> IDR</h3>
                    <p>
                        <?php echo CHtml::decode($model->deskripsi); 


                        ?>
                    </p>
                    <h4><u>Ukuran Yang tersedia</u></h4>
                    <p>
                        

 <?php   
     $uk = array();
     $ukur = TbUkuran::model()->findAllByAttributes(array('idbarang'=>$model->id));
     foreach ($ukur as $ukr){
       echo $ukr['ukuran']."<br>";
       
     }
   ?>
                    </p>
                </div>
                <div class="col-sm-2">
                    <?php
                    $this->widget(
                    'booster.widgets.TbButtonGroup',
                    array(
                        'context' => 'warning',
                        'justified' => true,
                        'size'=>'small',
                        'buttons' => array(
                            array('icon'=>'arrow-left'),
                            array('icon'=>'arrow-right'),
                              ),
                        )
                    );

                    ?>
                    <?php echo $this->renderPartial('_keranjang', array('model_keranjang'=>$model_keranjang,'model'=>$model)); ?>
                </div>
            </div>
	</div>
</div>

<div class="row">
    <div class="col-md-12">
    <h1>Produk <small>Sejenis</small></h1>
    <hr>
    </div>
</div>
