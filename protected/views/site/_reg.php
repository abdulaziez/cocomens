<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'site-reg',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldGroup($model,'user',array('widgetOptions'=>array('htmlOptions'=>array('maxlength'=>255)))); ?>

	<?php echo $form->passwordFieldGroup($model,'password',array('widgetOptions'=>array('htmlOptions'=>array('maxlength'=>255)))); ?>

	<div class="row">
				   <div class="col-sm-6">
						<?php echo $form->textFieldGroup($model,'namadepan',array('widgetOptions'=>array('htmlOptions'=>array('maxlength'=>255)))); ?>
				   </div>
				  <div class="col-sm-6">
						<?php echo $form->textFieldGroup($model,'namabelakang',array('widgetOptions'=>array('htmlOptions'=>array('maxlength'=>255)))); ?>
				  </div>	
	</div>

	<?php echo $form->textFieldGroup($model,'email',array('widgetOptions'=>array('htmlOptions'=>array('maxlength'=>255)))); ?>

	<?php echo $form->textFieldGroup($model,'telephon',array('widgetOptions'=>array('htmlOptions'=>array('maxlength'=>20)))); ?>
	
	<?php echo CHtml::activeLabel($model, 'validation'); ?>
	<?php $this->widget('application.extensions.recaptcha.EReCaptcha', 
			array('model'=>$model, 'attribute'=>'validation',
			'theme'=>'white', 'language'=>'en', 
			'publicKey'=>'6Ld54AkAAAAAACwmsSxBuBsDx1feTRNjfdImt5Ox')) ?>
	<?php echo CHtml::error($model, 'validation'); ?>
	<p>
	Dengan menekan tombol <span class="label label-success">Buat Akun Saya</span> , anda menyetujui kebijakan dan ketentuan kami, termasuk penggunaan kuki.
	</p>

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'success',
			'size' => 'large',
			'icon'=>'pencil',
			'label'=>$model->isNewRecord ? ' Buat akun saya' : ' Save',
		)); ?> 
</div>

<?php $this->endWidget(); ?>
