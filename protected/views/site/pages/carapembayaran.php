<h4>Cara Pembayaran</h4>
<div class="row">
<div class="col-md-6">
<ol>
<ul type="square"></ul>

<li>Masuk di Menu SHOP dan kli</li>
<li>Cari barang yang anda inginkan menurut kategori.</li>
<li>Klik barang yang anda inginkan. Pilih ukuran/Size, dan kuantitas sesuai dengan yang ingin Anda beli.</li>
<li>Lalu tekan tombol “Add to Cart”. Anda bisa memasukkan beberapa barang sekaligus dalam satu keranjang.</li>
<li>Klik Menu ” KERANJANG” untuk melanjutkan proses pembelian.</li>
<li>Klik “PROCEED TO CHECKOUT ” untuk melakukan pembelian.</li>
<li>Isilah data-data yang diperlukan untuk melakukan konfirmasi pembelian. Pastikan data yang diperlukan sudah terisi lengkap dan benar.</li>
<li>Jika Anda belum memiliki akun di Cocomens.com silahkan registrasi terlebih dahulu, jika Anda sudah terdaftar silahkan login. Jika Anda tidak ingin mendaftar pada Cocomens.com Anda dapat menekan tombol “MEMBUAT PESANAN” untuk melanjutkan proses pembelian.</li>
<li>Konfirmasi pembelian Anda dengan menekan tombol “confirm”.</li>
<li>Email konfirmasi pembelian akan dikirimkan kepada Anda.</li>
<li>Jika Anda melakukan pembayaran dengan metode Bank Transfer, harap melakukan konfirmasi pembayaran melalui member area (login terlebih dahulu) atau melalui email cocotshirt@gmail.com atau dengan konfirmasi Via SMS di NO 08114455919 (sms : no_id order anda/Nama/Alamat/Nama_Barang/Jumlah</li>
</ol>
</div>
<div class="col-md-6">
	
</div>
<br>

</div>
