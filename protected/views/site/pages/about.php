
<div class="container">
     <div class="row">
            <div class="col-md-3 col-md-offset-1"><br><br><br>
             <img src="images/logo.png" width="60%">   
            </div>
            <div class="col-md-8">
            <h3><b>Name Company</b></h3>
              <p>
                COCOMENS
             </p>

             <h3><b>Location</b></h3>
              <p>
                <address>
                MAKASSAR,<br>
                SULAWESI SELATAN,<br>
                1040 JL.RAPPOCINI RAYA LR. 11 B NO.2
                </address>
             </p>

               <h3><b>Contact</b></h3>
              <p><address>
                +628114455919<br>
                cocotshirt@gmail.com<br>
                PIN BB : 79D66E9A
                </address>
             </p>
 
            </div>
      </div>

 

<?php $collapse = $this->beginWidget('booster.widgets.TbCollapse'); ?>
<div class="row">
<div class="col-md-7">

<div class="panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
          <h4>Apa itu COCOMENS ?</h4>
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in">
      <div class="panel-body">
        Cocomens.com adalah fashion online asli kota makassar yang terletak Jl.Rappocini Raya Lr. 11 b No.2 kami melayani fashion untuk pria .Dengan konsep fashion yang fress dan mengikuti trend fashion terkini , dengan kualitas tinggi namun terjangkau.
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
          Bagaimana kwalitas produk COCOMENS ?
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse">
      <div class="panel-body">
        Kualitas 100% bahan lokal terbaik, dan kerapian jahitannya.
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
        Bagaimana layanan pengiriman COCOMENS ?
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse">
      <div class="panel-body">
     Kami melayani pesanan anda dimanapun anda berada, untuk wilayah kota makassar kami akan mengantar pesanan anda dengan kurir khusus dikantor maupun dirumah anda hanya dengan membayar jasa “Service Fitting on Location” Rp.15.000,- dan untuk luar makassar kami menggunakan jasa pelayanan pengiriman barang terbaik dan garansi pengembalian barang yang rusak.

      </div>
    </div>
  </div>

<div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapsefour">
         COCOMENS memiliki pengiriman gratis? ?
        </a>
      </h4>
    </div>
    <div id="collapsefour" class="panel-collapse collapse">
      <div class="panel-body">
      Ya ,kami memberikan gratis pengiriman dengan ketentuan :

        <li>Minimal pembelanjaan RP. 300.000-, untuk wilayah kota Makassar</li>

       <li> Minimal pembelanjaan RP. 500.000-, untuk wilayah bali,jawa,Kalimantan,Sulawesi</li>

		    <li>Minimal pembelanjaan RP. 1.000.000-, untuk wilayah papua</li>
      </div>
    </div>
  </div>

<br><br>








</div>



</div>
 <div class="col-md-5">
 <img src="">
	
 </div>

</div>
<?php $this->endWidget(); ?>




<div class="row">
  <div class="col-md-12">

<h4>Lokasi COCOMENS Dapat  di Lihat di Maps Berikut</h4>
<hr>

    <?php
//
// ext is your protected.extensions folder
// gmaps means the subfolder name under your protected.extensions folder
//  
Yii::import('ext.gmaps.*');
 
$gMap = new EGMap();


$gMap->setWidth(1150);
    $gMap->setHeight(500);
$gMap->zoom = 17;

$mapTypeControlOptions = array(
  'position'=> EGMapControlPosition::LEFT_BOTTOM,
  'style'=>EGMap::MAPTYPECONTROL_STYLE_DROPDOWN_MENU
);
 
$gMap->mapTypeControlOptions= $mapTypeControlOptions;
 
$gMap->setCenter(-5.1563255,119.4321781);

// Create GMapInfoWindows
$info_window_a = new EGMapInfoWindow('<div>I am a marker with custom image!</div>');
$info_window_b = new EGMapInfoWindow('Hey! I am a marker with label!');
 
$icon = new EGMapMarkerImage("http://google-maps-icons.googlecode.com/files/home.png");
 
$icon->setSize(25, 25);
$icon->setAnchor(16, 16.5);
$icon->setOrigin(0, 0);
 
// Create marker
//$marker = new EGMapMarker(-5.091256,119.5057226);
$marker = new EGMapMarkerWithLabel(-5.1563255,119.4321781, array('title' => 'Cocomens'));
$marker->addHtmlInfoWindow($info_window_a);
$gMap->addMarker($marker);
 
// Create marker with label
//$marker = new EGMapMarkerWithLabel(-5.0917730,119.5057228, array('title' => 'kantor PIP2BSul-Sel'));
 
$label_options = array(
  'backgroundColor'=>'yellow',
  'opacity'=>'0.75',
  'width'=>'100px',
  'color'=>'blue'
);
/*
// Two ways of setting options
// ONE WAY:
$marker_options = array(
  'labelContent'=>'$9393K',
  'labelStyle'=>$label_options,
  'draggable'=>true,
  // check the style ID 
  // afterwards!!!
  'labelClass'=>'labels',
  'labelAnchor'=>new EGMapPoint(22,2),
  'raiseOnDrag'=>true
);
 
$marker->setOptions($marker_options);
*/
 
// SECOND WAY:
$marker->labelContent= 'Cocomens';
$marker->labelStyle=$label_options;
$marker->draggable=true;
$marker->labelClass='labels';
$marker->raiseOnDrag= true;
 
$marker->setLabelAnchor(new EGMapPoint(0,0));
 
$marker->addHtmlInfoWindow($info_window_b);
 
$gMap->addMarker($marker);
 
// enabling marker clusterer just for fun
// to view it zoom-out the map
$gMap->enableMarkerClusterer(new EGMapMarkerClusterer());
 
$gMap->renderMap();
?>
<style type="text/css">
.labels {
   color: red;
   background-color: white;
   font-family: "Lucida Grande", "Arial", sans-serif;
   font-size: 10px;
   font-weight: bold;
   text-align: center;
   width: 40px;     
   border: 2px solid black;
   white-space: nowrap;
}
</style><br>

    
  </div>
  

</div>



  </div>

<br>


