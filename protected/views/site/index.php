<div class="container">
<?php
Yii::app()->clientScript->registerCoreScript('jquery'); //if you do not set yet
 
  //you have to put this code in your view file
  $this->widget('application.extensions.FlexPictureSlider.FlexPictureSlider',
  array('imageBlockSelector' => '#myslider', //the jquery selector
  ));
 
 
  //or with full custom parameters
  $this->widget('application.extensions.FlexPictureSlider.FlexPictureSlider',
  array(
    'imageBlockSelector' => '#myslider', //the jquery selector
    'widthSlider' => '1220', //or you can use jquery '$(window).width()/1.6',
    'heightSlider' => '600', //or you can use jquery '$(window).height()/1.6',
    'slideUnitSize' => 'px', //px or %
    'timeBetweenChangeSlider' => 4000,
    'slideRandomStart' => true, //only for version 1.0
    'timeDelayAnimation' => 1000, //the time before slider starts in miliseconds
    'sliderStartFrom' => 0, //if sliderSuffle is set false, only for version 1.1
    'sliderSuffle' => true, //suffle the pictures for random display, only for version 1.1
 
   )); 
  ?>
 
  
  <div id="myslider" >
  <?php
  echo CHtml::image(Yii::app()->request->baseUrl . '/images/conten/head2.png','alt 1');
  echo CHtml::image(Yii::app()->request->baseUrl . '/images/conten/11.png', 'alt 2');
  echo CHtml::image(Yii::app()->request->baseUrl . '/images/conten/head2.png', 'alt 3');
  echo CHtml::image(Yii::app()->request->baseUrl . '/images/conten/11.png', 'alt 4');

  ?>
  </div>
<br><br>


<center> 
<img src="images/conten/hr.png" width="80%"> 
</center>


<br><br>

<div class="row">
  <div class="col-md-4 col-md-offset-1">
   <h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sasaran dari Cocomens.com</h3>
  <ul>
  <li>Membentuk kekuatan pasar merek lokal untuk mendukung penguasaan pasar produk lokal di industri fashion di Indonesia.</li>
  <li>Mencintai dan menginspirasi fashion lokal untuk tumbuh secara luas dengan kualitas dan keunikannya.</li>
  <li>Membentuk sumber daya manusia dengan kreativitas yang tinggi ,menunjukkan bahwa produk lokal benar-benar memiliki kualitas terbaik.</li>
<ul>
</p>
  </div>
   <div class="col-md-5 col-md-offset-1">
  <h3>Cocomens ?</h3>
 Cocomens.com adalah fashion online asli kota makassar yang terletak Jl.Rappocini Raya Lr. 11 b No.2 ,
 kami melayani fashion untuk pria .Dengan konsep fashion yang fress dan mengikuti trend fashion terkini , 
 dengan kualitas tinggi namun terjangkau serta memberikan banyak pilihan untuk menyempurnakan penampilan Anda.
  </div>
</div>
<br><br>
<center> 
<img src="images/conten/hr2.png"  width="80%"> 
</center>

<div class="row">
<center><h3>Service Fitting on Location</h3></center>
<div class="col-md-10 col-sm-offset-2">
 <a href="#"><img src="images/conten/servis.png" width="40%"></a>
 <a href="#"><img src="images/conten/servis2.png" width="40%"></a>
</div>
<br><br>
<center>

<p>&nbsp;&nbsp;&nbsp;Kami memeiliki konsep penjualan produk COCO secara online dimana setiap pelanggan akan “Service Fitting on Location” 
jadi kami akan mengantarkan produk COCO sampai kerumah atau kantor anda untuk fitting produk yang telah dipilih.</p>
</center>

</div>
</div>
<br>

