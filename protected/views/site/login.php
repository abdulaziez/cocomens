<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>
<div class="container">
<div class="row">
	<div class="col-md-5">
		<h1>Member <small> login </small></h1>

		<p>Ooops..! Silahkan memasukkan User dan password akun anda terlebih dahulu.</p>


		<?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
			'id'=>'login-form',
			'enableClientValidation'=>true,
			'clientOptions'=>array(
				'validateOnSubmit'=>true,
			),
		)); ?>
				
				<?php echo $form->textFieldGroup($model,'username',array('widgetOptions'=>array('htmlOptions'=>array('maxlength'=>255)))); ?>
				<?php echo $form->errorSummary($model,'username'); ?>
					
				<?php echo $form->passwordFieldGroup($model,'password',array('widgetOptions'=>array('htmlOptions'=>array('maxlength'=>255)))); ?>
				<?php echo $form->errorSummary($model,'password'); ?>
						
				<?php echo $form->checkBoxGroup($model,'rememberMe'); ?>
			
				<div class="row">
					<div class="col-sm-4">
						<?php $this->widget('booster.widgets.TbButton', array(
								'buttonType'=>'submit',
								'context'=>'info',
								'size' => 'large',
								'icon' => 'open',
								'label'=>'Masuk',
						)); ?>
					</div>
					<div class="col-sm-6 col-sm-offset-1">
						<?php echo CHtml::link('Saya Lupa Pasword ?',array('index')); ?>
						<br>
						<?php echo CHtml::link('Saya Belum Punya Akun COCOMENS',array('register')); ?>
					</div>
				</div>

		<?php $this->endWidget(); ?>
	</div>
	<div class="col-md-7">
	
	</div>
</div>
</div>
<br>
