<?php
$this->breadcrumbs=array(
	'Site'=>array('index'),
	'Registrasi',
);
?>
<br>
<div class="container">
<div class="row">
	<div class="col-md-6">
		<img src="images/registrasi.png" width="100%">
		<br>
		<h1>Kenapa <small>Saya harus jadi member ?</small></h1>
		<hr>
		<p>
			Dengan melakukan registrasi anda telah memberikan kepercayaan kepada kami dan kepercayaan kami kepada anda untuk melakukan transaksi.			
		</p>
		<p>
			Dapatkan Informasi - informasi pakaian terbaru masakini dan keuntungan keuntungan menjadi member cocomens.
		</p>
		<h1>Kenali <small>Kami juga di?</small></h1>
		<hr>
		<p>
			Kami hadir lebih dekat dengan anda ..., ikuti juga informasi produk kami di : <br>
			<a href="https://id-id.facebook.com/people/Coco-Mens-Wear/100008204026575"><span class="fa fa-lg fa-facebook-square"></span> Coco Mens Wear</a><br>
			<a href="#"><span class="fa fa-lg fa-twitter-square"></span> @Cocomensmakassar</a><br>
			<a href="#"><span class="fa fa-lg fa-google-plus-square"></span> Cocomens</a><br>
		</p>
		
	</div>
	<div class="col-md-5 col-md-offset-1">
		<h1>Cocomens <small>Registrasi Member</small></h1>
		<hr>
		<?php echo $this->renderPartial('_reg', array('model'=>$model)); ?>
	</div>
	
</div>
</div>
<br>