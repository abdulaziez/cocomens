
<div class="container">
<div class="row">
<div class="col-md-7">
<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form CActiveForm */

$this->pageTitle=Yii::app()->name . ' - Contact Us';
$this->breadcrumbs=array(
	'Contact',
);
?>

<h1>Hubungi Kami</h1><br>

<?php if(Yii::app()->user->hasFlash('contact')): ?>

<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('contact'); ?>
</div>

<?php else: ?>

<p>
Jika Anda memiliki pertanyaan bisnis atau pertanyaan yang menyangkut tentang COCOMENS , silakan isi form berikut untuk menghubungi kami Terima kasih.

</p>

<div class="form">
<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'contact-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<p class="note">Kolom dengan <span class="required">*</span> harus diisi.</p>

	<?php echo $form->errorSummary($model); ?>
    <?php echo $form->textFieldGroup($model,'name',array('widgetOptions'=>array('htmlOptions'=>array('maxlength'=>255)))); ?>
	<?php echo $form->textFieldGroup($model,'email',array('widgetOptions'=>array('htmlOptions'=>array('maxlength'=>255)))); ?>
	<?php echo $form->textFieldGroup($model,'subject',array('widgetOptions'=>array('htmlOptions'=>array('maxlength'=>128)))); ?>
	<?php echo $form->textAreaGroup($model,'body',array('widgetOptions'=>array('htmlOptions'=>array('maxlength'=>255)))); ?>
	

	<?php echo CHtml::activeLabel($model, 'verifyCode'); ?>
	<?php $this->widget('application.extensions.recaptcha.EReCaptcha', 
			array('model'=>$model, 'attribute'=>'verifyCode',
			'theme'=>'white', 'language'=>'en', 
			'publicKey'=>'6Ld54AkAAAAAACwmsSxBuBsDx1feTRNjfdImt5Ox')) ?>
	<?php echo CHtml::error($model, 'verifyCode'); ?>
	</div>
	<br>

	<div class="row buttons">
	<div class="col-md-1 col-md-offset-1">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'info',
			'size' => 'medium',
			'icon'=>'pencil',
			'label'=>'kirim' ,
		)); ?>
		</div> 
   </div>



	

<?php $this->endWidget(); ?>

<!-- form -->

<?php endif; ?>

</div>
<div class="col-md-4 col-md-offset-1"><br><br><br><br><br><br><br><br><br>
<h4>Head Office</h4><br>
<pre>
<address>
Address :Jl.Rappocini Raya Lr. 11 b No.2
Kota    :MAKASSAR,Sulawesi Selatan, 1040
Tel     :+628114455919
E-Mail  :cocotshirt@gmail.com
pin bb  :79D66E9A
</address>
</pre>
</div>
</div>
</div>
<br><br>