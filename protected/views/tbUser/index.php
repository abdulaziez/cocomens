<?php
$this->breadcrumbs=array(
	'Tb Users',
);

$this->menu=array(
array('label'=>'Create TbUser','url'=>array('create')),
array('label'=>'Manage TbUser','url'=>array('admin')),
);
?>

<h1>Tb Users</h1>

<?php $this->widget('booster.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
