<?php
$this->breadcrumbs=array(
	'Tb Users'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List TbUser','url'=>array('index')),
array('label'=>'Create TbUser','url'=>array('create')),
array('label'=>'Update TbUser','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete TbUser','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage TbUser','url'=>array('admin')),
);
?>

<h1>View TbUser #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'user',
		'password',
		'namadepan',
		'namabelakang',
		'namaperusahaan',
		'alamat',
		'kota',
		'provensi',
		'kodepos',
		'email',
		'telephon',
		'level_id',
		'kode',
		'status',
),
)); ?>
