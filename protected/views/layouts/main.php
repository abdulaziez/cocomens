<?php /* @var $this Controller */?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="en">
<?php /*
	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print">
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection">
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css">
*/ ?>
	
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

    <div  id="page" class="container-fluid">

	<div id="header">
		<div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
	</div><!-- header -->

	<div id="mainmenu">
		<?php /* $this->widget('zii.widgets.CMenu',array(
			'items'=>array(
				array('label'=>'Home', 'url'=>array('/site/index')),
				array('label'=>'About', 'url'=>array('/site/page', 'view'=>'about')),
				array('label'=>'Contact', 'url'=>array('/site/contact')),
				array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
			),
		)); */
		$this->widget('booster.widgets.TbNavbar',
		array(
			'brand' => 'COCOMENS',
			//'type' => 'inverse',
			'brandOptions' => array('style' => 'width:auto;margin-left: 0px;'),
			'fixed' => 'top',
			//'fluid' => true,
			//'htmlOptions' => array('style' => 'position:absolute'),
			'items' => array(
            array(
                'class' => 'booster.widgets.TbMenu',
            	'type' => 'navbar',
				'items'=>array(
					array('label'=>'Toko', 'url'=>array('/site/index')),
					array('label'=>'Produk', 'url'=>array('/tbBarang/index')),
					array('label'=>'Tentang Kami', 'url'=>array('/site/page', 'view'=>'about')),
					array('label'=>'Hubungi Kami', 'url'=>array('/site/contact')),
					array('label'=>'Keranjang Belanja', 'url'=>array('/tbKeranjang/index')),
					)),
					
			array(		
					'class' => 'booster.widgets.TbMenu',
					'type' => 'navbar',
					'htmlOptions' => array('class' => 'pull-right'),
					'items' => array(
						array('label'=>'Masuk', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
						array('label' => 'Buat Akun', 'url' => array('site/register'), 'visible'=>Yii::app()->user->isGuest),
						array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),
				),
			),
			),
		)
		);
		?>
	</div><!-- mainmenu -->
	<?php if(isset($this->breadcrumbs)):?>
        <br><br><br>
                <div class="container">
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
                <hr>
                </div>
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div>

</div><!-- page -->

<div class="container-fluid">
    <div class="row bg-primary" >
		
        <div class="col-lg-3">
            <div class="row">
			<div class="col-sm-offset-2">
				<h3>Perusahaan</h3>
				<hr>
				<div class="row">
				   <div class="col-md-3">
					  <center> <img src="images/logo-putih.png" width="135%" > </center><br>
				   </div>
				  <div class="col-md-9">
					Toko online yang menawarkan brand fashion khusus pria dapatkan produk berkualitas dari kami.<br>
				  </div>	
			   </div>
				
				<address>
				   <strong> Contact :</strong><br>
				   +628114455919<br>
				   cocotshirt@gmail.com<br>
				   PIN BB : 79D66E9A
				 </address>
			 </div>
		</div>
            </div>
          <div class="col-lg-3">
              <div class="row">
			<div class="col-md-12">
				 <h3>Tentang Cocomens</h3>
				 <hr>
				 COCOMENS adalah Toko Online yang menyediakan fashion khusus pria, 
				 dengan menawarkan produk berkualitas dengan harga terjangkau.
				 Kami menyediakan produk-produk yang selalu mengikuti trend fashion terbaru yang memberikan banyak pilihan untuk menyempurnakan penampilan Anda. 
			</div>
              </div>
	  </div>
          <div class="col-lg-3">
              <div class="row">
                    <div class="col-md-12">
				<h3>Info</h3>
					<hr>
<?php

$this->widget(
    'booster.widgets.TbMenu',
	
    array(
		'type' => 'list',
		'items' => array(
			array(
					'label' => 'Cara Pembayaran',
					'url'=>array('/site/page', 'view'=>'carapembayaran'),
			),
			array(
					'label' => 'Buat Desain Pakaian Sendiri',
					'url'=>array('/site/page', 'view'=>'carapembayaran'),
			),
			array(
					'label' => 'Produk',
					'url'=>array('/site/page', 'view'=>'carapembayaran'),
			),
			array(
					'label' => 'Promo',
					'url'=>array('/site/page', 'view'=>'carapembayaran'),
			),
		),
	));
     
 ?>

				
			</div>
		  </div>
          </div>  
	      <div class="col-md-3">
                  <div class="row">
			<div class="col-sm-10">
				<h3>Ikuti Kami</h3>
					<hr>
					<a href="https://id-id.facebook.com/people/Coco-Mens-Wear/100008204026575"><img src="images/fb.png" width="20%"></a>
					<a href=""><img src="images/twitter.png" width="20%"></a>
					<a href=""><img src="images/gmail.png" width="20%"></a>
			</div>
                  </div>
	      </div>
	</div>

	<div id="footer" >
	<div class="row bg-success">
    	
		<center>Copyright &copy; <?php echo date('Y'); ?> by coconut-labs. All Rights Reserved.</center>
	</div>
	</div>

	<!-- footer -->

</div>

</body>
</html>
