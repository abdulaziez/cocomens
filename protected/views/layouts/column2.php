<?php /* @var $this Controller */ ?>

        
	
<?php $this->beginContent('//layouts/main'); ?>
<div class="container">
    <div class="row">
        <div class="col-sm-10">
                <div id="content">
                        <?php echo $content; ?>
                </div><!-- content -->
        </div>
        
        <?php if(isset($this->menu)):?>
        <div class="col-md-2 last">
                <div class="row" id="sidebar" >
                    <div class="col-sm-offset-1 col-sm-11 ">
                        <?php

                        $this->widget(
                        'booster.widgets.TbMenu',
                    array(
                        'type' => 'list',
                        'items' => $this->menu,
                            )
                        );

                        ?>
                    </div>
                </div><!-- sidebar -->
        </div>
        <?php endif?>
    </div>
</div>
<?php $this->endContent(); ?>