<?php

class TbBarangController extends Controller
{
/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/
public $layout='cocoadmin.views.layouts.column2';

/**
* @return array action filters
*/
public function filters()
{
return array(
'accessControl', // perform access control for CRUD operations
);
}

/**
* Specifies the access control rules.
* This method is used by the 'accessControl' filter.
* @return array access control rules
*/
public function accessRules()
{
return array(
array('allow', // allow authenticated user to perform 'create' and 'update' actions
'actions'=>array('create','update','admin','delete','index','view'),
'users'=>array('@'),
),
array('deny',  // deny all users
'users'=>array('*'),
),
);
}

/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/
public function actionView($id)
{
 $model_keranjang=new TbKeranjang();   
 $idbrg = $id;
 if(isset($_POST['TbKeranjang']))
{
 
 $model_keranjang->attributes=$_POST['TbKeranjang'];
 $model_keranjang->ids = Yii::app()->session['cart_code'];
 $model_keranjang->idbarang = $idbrg;
 $hrg = TbBarang::model()->findByPk($idbrg);
 $model_keranjang->jumlahharga = $hrg->harga * $model_keranjang->jumlahpembelian;
 
 if ($model_keranjang->save()){
     
  //$this->redirect(array('view','id'=>$model->id));  
 }
     
 }
 
$this->render('view',array(
'model'=>$this->loadModel($id),'model_keranjang'=>$model_keranjang,
));
}


/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
public function actionCreate()
{
$model=new TbBarang;

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

if(isset($_POST['TbBarang']))
{
$model->attributes=$_POST['TbBarang'];
if($model->save())
Yii::app()->session['idbrg'] = $model->id;
$this->redirect();
}

$this->render('create',array(
'model'=>$model,
));
}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
public function actionUpdate($id)
{
$model=$this->loadModel($id);

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

if(isset($_POST['TbBarang']))
{
$model->attributes=$_POST['TbBarang'];
if($model->save())
$this->redirect(array('view','id'=>$model->id));
}

$this->render('update',array(
'model'=>$model,
));
}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
public function actionDelete($id)
{
if(Yii::app()->request->isPostRequest)
{
// we only allow deletion via POST request
$this->loadModel($id)->delete();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
if(!isset($_GET['ajax']))
$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
}
else
throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
}

/**
* Lists all models.
*/
public function actionIndex()
{
$dataProvider=new CActiveDataProvider('TbBarang');
$this->render('index',array(
'dataProvider'=>$dataProvider,
));
}

/**
* Manages all models.
*/
public function actionAdmin()
{
$model=new TbBarang('search');
$model->unsetAttributes();  // clear any default values
if(isset($_GET['TbBarang']))
$model->attributes=$_GET['TbBarang'];

$this->render('admin',array(
'model'=>$model,
));
}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadModel($id)
{
$model=TbBarang::model()->findByPk($id);
if($model===null)
throw new CHttpException(404,'The requested page does not exist.');
return $model;
}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='tb-barang-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}


public function init(){
        /* kode jika sesion cartcode tidak di definisikan*/
            if(!isset(Yii::app()->session['cart_code'])){
                /*membuat kode unik untuk cart kode*/
                Yii::app()->session['cart_code'] = $this -> getUniqueCode();
            }
        }
    
    /*function untuk mengenerate kode chart*/
        public function randomCode(){
            /*buat karakter yang akan di random*/
            $RANDCODE = "abcdefghijklmnopqrstuvwxyz023456789";

            /*untuk mengacak kode*/
            srand((double)  microtime()*1000000);
            $i=0;
            $generateCode = '';
            /*perulangan sebanyak 7 kali*/
            while ($i <= 7){
                /*kode random*/
                $num = rand() % 34;
                /*ambil karakter dari $RANDCODE dengan 
                 * posisinya di acak oleh $num
                 */
                $tmp = substr($RANDCODE, $num, 1);
                /*hasil generate code disimpan di $generatecode*/
                $generateCode = $generateCode.$tmp;
                $i++;
            }
        return strtoupper($generateCode);
        }
        
        /*function untuk generate random kode*/
        public function rerandomCode(){
            /*buat karakter yang akan di random */
            $RANDCODE = "023456789abcdefghijklmnopqrstuvwxyz";
            /*untuk mengacak kode*/
            srand((double)microtime()*1000000);
            $i =0;
            $generateCode = '';
            /*lopping sebanyak 7 kali */
            while($i <= 7){
                /*kode random*/
                $num = rand()%34;
                /*ambil karakter dari $RANDCODE dengan
                 * posisinya diacak oleh $num
                 */
                $tmp = substr($RANDCODE,$num, 1);
                /*hasil generate kode ditampung ke $generateCode*/
                $generateCode = $generateCode . $tmp;
                $i++;
            }
            return strtoupper($generateCode);
        }
        
        /*untuk generate order code*/
        public function orderCode(){
            /*karakter yang akan di random*/
            $RANDCODE = "023456789";
            /*untuk mengacak kode*/
            srand((double)  microtime()*1000000);
            $i = 0;
            $generateCode='';
            /*looping sebanyak 5 kali*/
            while($i <= 5){
                /*kode random*/
                $num = rand()%5;
                /*ambil karakter dari $RANDCODE dengan 
                 * posisinya diacak oleh $num
                 */
                $tmp = substr($RANDCODE,$num, 1);
                /*hasil generate code ditampung ke $generateCode*/
                $generateCode = $generateCode . $tmp;
                $i++;
            }
            /*kembalikan nilai function ke $generateCode*/
            return strtoupper($generateCode);
        }
        
        /*untuk menggabungkan kode unik yang telah kita buat*/
        public function getUniqueCode(){
            return $this->randomCode().'-'.$this->rerandomCode();
        }
}
