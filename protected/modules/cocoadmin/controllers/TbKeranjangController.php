<?php

class TbKeranjangController extends Controller
{
	public function actionIndex()
	{
                $model_keranjang = new TbKeranjang();
                $model_order = new TbOrder();
                $model_penjualan= new TbPenjualan();
                $model_ukuran = new TbUkuran();
                $dataProvider = TbKeranjang::model()->findAllByAttributes(
                        array('ids'=>Yii::app()->session['cart_code']));
                
                
                
		$this->render('index',array(
                'dataProvider'=>$dataProvider ,'model_keranjang'=>$model_keranjang,'model_order'=>$model_order,'model_penjualan'=>$model_penjualan,'model_ukuran'=>$model_ukuran,
                ));
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
        
        
}