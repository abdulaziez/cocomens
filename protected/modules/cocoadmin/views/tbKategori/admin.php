<?php
$this->breadcrumbs=array(
	'Tb Kategoris'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List TbKategori','url'=>array('index')),
array('label'=>'Create TbKategori','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('tb-kategori-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Kelolah Data Kategori</h1>


<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'tb-kategori-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'id',
		'kategori',
array(
'class'=>'booster.widgets.TbButtonColumn',
),
),
)); ?>
