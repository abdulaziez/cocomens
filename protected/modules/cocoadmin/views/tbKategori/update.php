<?php
$this->breadcrumbs=array(
	'Tb Kategoris'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List TbKategori','url'=>array('index')),
	array('label'=>'Create TbKategori','url'=>array('create')),
	array('label'=>'View TbKategori','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage TbKategori','url'=>array('admin')),
	);
	?>

	<h1>Update TbKategori <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>