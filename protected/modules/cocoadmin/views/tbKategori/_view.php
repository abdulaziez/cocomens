<div class="row">
        <div class="col-md-2">
            <b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?></b><br>
            <b><?php echo CHtml::encode($data->getAttributeLabel('kategori')); ?></b>
        </div>
        <div class="col-md-5">
            : <?php echo "KT-".CHtml::encode($data->id); ?><br>
            : <?php echo CHtml::encode($data->kategori); ?>
        </div>
        <div class="col-md-2">
        <?php $this->widget('booster.widgets.TbButton', array(
                        'label'=>' Edit Kategori',
                        'icon'=>'pencil','url'=>'#',
                        
                )); ?>
        </div>
        <div class="col-md-2">
         <?php $this->widget('booster.widgets.TbButton', array(
                        'label'=>' Hapus Kategori',
                        'icon'=>'trash','url'=>'#',
                        'htmlOptions'=>array(
                            'submit'=>array(
                                'delete','id'=>$data->id),
                            'confirm'=>'Apakah Anda Yakin Akan Menghapus Data Kategori Ini?'
                        ),
                )); ?>
        </div>
    </div>
<hr>