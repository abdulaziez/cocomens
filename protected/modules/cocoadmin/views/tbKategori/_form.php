
<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'tb-kategori-form',
	'enableAjaxValidation'=>false,
        'htmlOptions'=>array('class'=>'form-inline'),
)); ?>
    <?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldGroup($model,'kategori',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambahkan' : 'Simpan',
		)); ?>

<?php $this->endWidget(); ?>
