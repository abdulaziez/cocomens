<?php
$this->breadcrumbs=array(
	'Tb Kategoris'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List TbKategori','url'=>array('index')),
array('label'=>'Manage TbKategori','url'=>array('admin')),
);
?>

<h2>Tambahkan Data Kategori</h2>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
<hr>

<h2>List Data Kategori</h2>
<?php $this->widget('booster.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>