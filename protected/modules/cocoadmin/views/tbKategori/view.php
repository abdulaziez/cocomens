<?php
$this->breadcrumbs=array(
	'Tb Kategoris'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List TbKategori','url'=>array('index')),
array('label'=>'Create TbKategori','url'=>array('create')),
array('label'=>'Update TbKategori','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete TbKategori','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage TbKategori','url'=>array('admin')),
);
?>

<h1>View TbKategori #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'kategori',
),
)); ?>
