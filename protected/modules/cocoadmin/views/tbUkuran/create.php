<?php
$this->breadcrumbs=array(
	'Tb Ukurans'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List TbUkuran','url'=>array('index')),
array('label'=>'Manage TbUkuran','url'=>array('admin')),
);
?>

<h1>Create TbUkuran</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>