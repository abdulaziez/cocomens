
<?php
$this->breadcrumbs=array(
	'TbBarang',
);

$this->menu=array(
array('label' => 'Produk Lainya','itemOptions' => array('class' => 'nav-header')),
array('label'=>'Create TbBarang','url'=>array('create')),
array('label'=>'Manage TbBarang','url'=>array('admin')),
);
?>

<h1>Tb Barangs</h1>

<?php $this->widget(
    'booster.widgets.TbThumbnails',
    array(
        'dataProvider' => $dataProvider,
        'template' => "{items}\n{pager}",
        'itemView' => '_view',
    )
);
?>
