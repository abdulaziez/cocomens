<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'tb-keranjang-form',
	'enableAjaxValidation'=>false,
)); ?>

<h3>Order <small>Sekarang</small></h3>
<hr>
<?php echo $form->errorSummary($model_keranjang);

?>
        <?php echo $form->dropDownListGroup(
			$model_keranjang,
			'idukuran',
			array(
				'wrapperHtmlOptions' => array(
					'class' => 'col-sm-5',
				),
				'widgetOptions' => array(
					'data' => CHtml::listData(TbUkuran::model()->findAllByAttributes(array('idbarang'=>$model->id)),'id','ukuran'),
					'htmlOptions' => array(),
				)
			)
	); ?>

        <?php echo $form->dropDownListGroup(
			$model_keranjang,
			'jumlahpembelian',
			array(
				'wrapperHtmlOptions' => array(
					'class' => 'col-sm-5',
				),
				'widgetOptions' => array(
					'data' => array('Something ...', '1', '2', '3', '4', '5'),
					'htmlOptions' => array(),
				)
			)
		); ?>
                 
<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model_keranjang->isNewRecord ? 'Order' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>