<?php
$this->breadcrumbs=array(
	'Tb Barangs'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List TbBarang','url'=>array('index')),
	array('label'=>'Create TbBarang','url'=>array('create')),
	array('label'=>'View TbBarang','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage TbBarang','url'=>array('admin')),
	);
	?>

	<h1>Update TbBarang <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>