<?php /*	
	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idkategori')); ?>:</b>
	<?php echo CHtml::encode($data->idkategori); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_barang')); ?>:</b>
	<?php echo CHtml::encode($data->nama_barang); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('stok_barang')); ?>:</b>
	<?php echo CHtml::encode($data->stok_barang); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('harga')); ?>:</b>
	<?php echo CHtml::encode($data->harga); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ukuran')); ?>:</b>
	<?php echo CHtml::encode($data->ukuran); ?>
	<br />
*/


	?>

     <?php   
     $dr = array();
     $foto = TbFoto::model()->findAllByAttributes(array('idbarang'=>$data->id));
     foreach ($foto as $d){
       
       $dr[] = $d['id'].'-'.$d['foto'];
   }
   ?>

<div class="col-sm-5 col-md-3">
    <div class="thumbnail">
<?php $kategori = TbKategori::model()->findByPk($data->idkategori);?>
        <div class="caption">
			<span class="label label-danger"><?php echo CHtml::encode($kategori->kategori);?></span>
<?php	  		echo CHtml::link("<img src='images/produk/".$dr[0]."' alt='' width='235' height='335' id='".CHtml::encode($data->id)."' onmouseenter='a".CHtml::encode($data->id)."()' onmouseleave='b".CHtml::encode($data->id)."()'>",array('view','id'=>$data->id)); ?>
                        <div class="carousel-caption" id=<?php echo CHtml::encode($data->id)."cr"?> >
				<h4><?php echo CHtml::encode($data->nama_barang); ?></h4>
				<p>Harga : <?php echo CHtml::encode($data->harga); ?> IDR</p>
			</div>
        </div>
    </div>
</div>
<script>
function a<?php echo CHtml::encode($data->id)?>() {
    <?php if(count($dr)>=2){ ?>
    document.getElementById("<?php echo CHtml::encode($data->id)?>").src = "images/produk/<?php echo $dr[1]?>";
    <?php } ?>
}

function b<?php echo CHtml::encode($data->id)?>() {
    document.getElementById("<?php echo CHtml::encode($data->id)?>").src = "images/produk/<?php echo $dr[0]?>";
}
</script>

