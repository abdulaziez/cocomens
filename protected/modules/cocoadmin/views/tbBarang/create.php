<?php
$this->breadcrumbs=array(
	'Tb Barangs'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List TbBarang','url'=>array('index')),
array('label'=>'Manage TbBarang','url'=>array('admin')),
);
?>

<h1>Create TbBarang</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>