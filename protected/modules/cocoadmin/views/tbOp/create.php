<?php
$this->breadcrumbs=array(
	'Tb Ops'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List TbOp','url'=>array('index')),
array('label'=>'Manage TbOp','url'=>array('admin')),
);
?>

<h1>Create TbOp</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>