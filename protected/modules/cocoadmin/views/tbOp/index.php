<?php
$this->breadcrumbs=array(
	'Tb Ops',
);

$this->menu=array(
array('label'=>'Create TbOp','url'=>array('create')),
array('label'=>'Manage TbOp','url'=>array('admin')),
);
?>

<h1>Tb Ops</h1>

<?php $this->widget('booster.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
