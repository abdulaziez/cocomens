<?php
$this->breadcrumbs=array(
	'Tb Ops'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List TbOp','url'=>array('index')),
array('label'=>'Create TbOp','url'=>array('create')),
array('label'=>'Update TbOp','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete TbOp','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage TbOp','url'=>array('admin')),
);
?>

<h1>View TbOp #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'username',
		'password',
		'level_id',
		'has',
),
)); ?>
