<?php
$this->breadcrumbs=array(
	'Tb Ops'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List TbOp','url'=>array('index')),
	array('label'=>'Create TbOp','url'=>array('create')),
	array('label'=>'View TbOp','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage TbOp','url'=>array('admin')),
	);
	?>

	<h1>Update TbOp <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>