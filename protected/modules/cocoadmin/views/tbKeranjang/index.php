<?php
/* @var $this TbKeranjangControllerController */

$this->breadcrumbs=array(
	'Keranjang',
);
?>
<div class="container">
    <div class="row alert alert-success">
        <div class="col-md-5">
            <h4> Produk di Pesan</h4>
    </div> 
        <div class="col-md-1">
         <h4>QTY</h4>
    </div>
        <div class="col-md-3">
       <h4> Jumlah Harga  </h4>  
    </div>
    </div><hr>
<?php 
foreach ($dataProvider  as $data):
?>
    
    <div class="row">
        <div class="col-md-2">
     <?php   
     $dr = array();
     $foto = TbFoto::model()->findAllByAttributes(array('idbarang'=>$data['idbarang']));
     foreach ($foto as $d){
       
       $dr[] = 'images/produk/'.$d['id'].'-'.$d['foto'];
     }
     echo "<img src='".$dr[0]."' alt='image' class='img-rounded col-sm-12'>";
     ?>
        </div> 
        <div class="col-md-3">
       <?php $br=TbBarang::model()->findByPk($data['idbarang']); 
         echo "<h3> $br->nama_barang </h3>";
         
         $ukuran=  TbUkuran::model()->findByPk($data['idukuran']);
         echo "<label class='label label-info'>Ukuran : $ukuran->ukuran</label>";
         
         echo "<h4>Rp. $br->harga ,-</h4>";
       ?>
         </div> 
        <div class="col-md-1">
   <?php $model_keranjang->jumlahpembelian = $data['jumlahpembelian'];
    echo $this->renderPartial('_view',array('model_keranjang'=>$model_keranjang));  
    ?>
        </div>
        <div class="col-md-3">
            <?php echo "<h3>Rp. ". $br->harga * $model_keranjang->jumlahpembelian .",-</h3>"; ?>
        </div>    
    
    
    
    </div><hr>
<?php
	endforeach;
?>    
</div>
