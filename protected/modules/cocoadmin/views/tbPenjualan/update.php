<?php
$this->breadcrumbs=array(
	'Tb Penjualans'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List TbPenjualan','url'=>array('index')),
	array('label'=>'Create TbPenjualan','url'=>array('create')),
	array('label'=>'View TbPenjualan','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage TbPenjualan','url'=>array('admin')),
	);
	?>

	<h1>Update TbPenjualan <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>