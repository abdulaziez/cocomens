<?php
$this->breadcrumbs=array(
	'Tb Users'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List TbUser','url'=>array('index')),
	array('label'=>'Create TbUser','url'=>array('create')),
	array('label'=>'View TbUser','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage TbUser','url'=>array('admin')),
	);
	?>

	<h1>Update TbUser <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>