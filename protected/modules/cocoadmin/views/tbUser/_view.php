<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user')); ?>:</b>
	<?php echo CHtml::encode($data->user); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('password')); ?>:</b>
	<?php echo CHtml::encode($data->password); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('namadepan')); ?>:</b>
	<?php echo CHtml::encode($data->namadepan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('namabelakang')); ?>:</b>
	<?php echo CHtml::encode($data->namabelakang); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('namaperusahaan')); ?>:</b>
	<?php echo CHtml::encode($data->namaperusahaan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alamat')); ?>:</b>
	<?php echo CHtml::encode($data->alamat); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('kota')); ?>:</b>
	<?php echo CHtml::encode($data->kota); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('provensi')); ?>:</b>
	<?php echo CHtml::encode($data->provensi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kodepos')); ?>:</b>
	<?php echo CHtml::encode($data->kodepos); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telephon')); ?>:</b>
	<?php echo CHtml::encode($data->telephon); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('level_id')); ?>:</b>
	<?php echo CHtml::encode($data->level_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kode')); ?>:</b>
	<?php echo CHtml::encode($data->kode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	*/ ?>

</div>