<?php /* @var $this Controller */ ?>

<?php $this->beginContent('cocoadmin.views.layouts.main'); ?>

<div class="row">
<div class="col-md-9">
	<div id="content">
		<?php echo $content; ?>
	</div><!-- content -->
</div>
<div class="col-md-3 last">
	<div id="sidebar">
	<?php
		$this->widget('booster.widgets.TbMenu',
                    array(
                        'type' => 'list',
                        'items' => $this->menu,
                        ));
	?>
	</div><!-- sidebar -->
</div>
</div>
<?php $this->endContent(); ?>
