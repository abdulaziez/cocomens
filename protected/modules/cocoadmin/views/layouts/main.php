<?php /* @var $this Controller */?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="en">
	
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div  class="container-fluid">
       
	<div id="header">
		<div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
	</div><!-- header -->
        
	<div id="mainmenu">
		<?php 
		$this->widget('booster.widgets.TbNavbar',
		array(
			'brand' => 'CocoAdmin | Dasbord',
			//'type' => 'inverse',
			'brandOptions' => array('style' => 'width:auto;margin-left: 0px;'),
			'fixed' => 'top',
			//'fluid' => true,
			'htmlOptions' => array('style' => 'position:fixed'),
			'items' => array(
    			
			array(		
					'class' => 'booster.widgets.TbMenu',
					'type' => 'navbar',
					'htmlOptions' => array('class' => 'pull-right'),
					'items' => array(
						array('label'=>'Masuk', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
						array('label' => 'Buat Akun', 'url' => array('site/register'), 'visible'=>Yii::app()->user->isGuest),
						array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),
				),
			),
			),
		)
		);
		?>
	</div><!-- mainmenu -->
        
        <div class="row">
            <div class="col-lg-2">
                <br><br><br>
                <div class="row">
                    <div class="col-md-11 col-md-offset-1">
          <?php $this->widget(
    'booster.widgets.TbMenu',
    array(
        'type' => 'list',
        'items' => array(
            array(
                'label' => 'Administrator',
                'itemOptions' => array('class' => 'nav-header')
            ),
            array(
                'label' => 'Dasbord',
                'url' => '#',
                'itemOptions' => array('class' => 'active')
            ),
            array('label' => 'Lihat Website', 'url'=>array('/site/index')),
            array('label' => 'Profil Admin', 'url' => '#'),
            array(
                'label' => 'Manajemen Website',
                'itemOptions' => array('class' => 'nav-header')
            ),
            array('label' => 'Produk', 
                  'items'=> array(
                      array('label' => 'Kategori Produk', 'url' =>array('tbKategori/admin')),
                      array('label' => 'Data Produk', 'url' =>array('tbBarang/admin')),
                      array('label' => 'Foto Produk', 'url' =>array('tbFoto/admin')),
                      array('label' => 'Ukuran Produk', 'url' =>array('tbUkuran/admin')),
                  )),
            
            '',
            array(
                'label' => 'Harga Bahan',
                'itemOptions' => array('class' => 'nav-header')
            ),
            array('label' => 'User', 'url' =>array('tbuser/admin')),
            array('label' => 'Operator', 'url' =>array('tbop/admin')),
            array('label' => 'Help', 'url' => '#'),
        )
    )
);?>
                    </div>
                </div>
            </div>
            <div class="col-lg-10">
                
	<?php if(isset($this->breadcrumbs)):?>
        <br><br><br>
               
		<?php $this->widget('booster.widgets.TbBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
                <hr>
 
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div>
            </div>
        </div>
</div><!-- page -->

<div class="container-fluid">
    

            <div class="row bg-primary">
                <div class="col-lg-12">
                    <small>  
                        Copyright &copy; <?php echo date('Y'); ?> PIP2B Sul-sel | All Rights Reserved.
                    </small>
                    <small class="pull-right">      
                        <?php echo Yii::powered(); ?> | created by Seno Apriliyadi &AMP; Masyudin
                    </small>
                </div>
            </div>

	<!-- footer -->

</div>

</body>
</html>
