<?php
$this->breadcrumbs=array(
	'Tb Orders'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List TbOrder','url'=>array('index')),
array('label'=>'Manage TbOrder','url'=>array('admin')),
);
?>

<h1>Create TbOrder</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>