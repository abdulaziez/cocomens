<?php
$this->breadcrumbs=array(
	'Tb Orders',
);

$this->menu=array(
array('label'=>'Create TbOrder','url'=>array('create')),
array('label'=>'Manage TbOrder','url'=>array('admin')),
);
?>

<h1>Tb Orders</h1>

<?php $this->widget('booster.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
