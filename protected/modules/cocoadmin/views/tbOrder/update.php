<?php
$this->breadcrumbs=array(
	'Tb Orders'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List TbOrder','url'=>array('index')),
	array('label'=>'Create TbOrder','url'=>array('create')),
	array('label'=>'View TbOrder','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage TbOrder','url'=>array('admin')),
	);
	?>

	<h1>Update TbOrder <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>