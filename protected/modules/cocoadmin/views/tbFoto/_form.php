<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'tb-foto-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array(
        'enctype' => 'multipart/form-data'),
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->dropDownListGroup(
			$model,
			'idbarang',
			array(
				'wrapperHtmlOptions' => array(
					'class' => 'col-sm-5',
				),
				'widgetOptions' => array(
					'data' => CHtml::listData(TbBarang::model()->findAll(),'id','nama_barang'),
					'htmlOptions' => array(),
				)
			)
		); ?>
	
	<?php echo $form->fileFieldGroup($model,'foto',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Simpan Data' : 'Save',
		)); 
       
        ?>
</div>

<?php $this->endWidget(); ?>
