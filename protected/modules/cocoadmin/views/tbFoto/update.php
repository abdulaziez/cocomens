<?php
$this->breadcrumbs=array(
	'Tb Fotos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List TbFoto','url'=>array('index')),
	array('label'=>'Create TbFoto','url'=>array('create')),
	array('label'=>'View TbFoto','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage TbFoto','url'=>array('admin')),
	);
	?>

	<h1>Update TbFoto <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>