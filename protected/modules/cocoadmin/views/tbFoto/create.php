<?php
$this->breadcrumbs=array(
	'Tb Fotos'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List TbFoto','url'=>array('index')),
array('label'=>'Manage TbFoto','url'=>array('admin')),
);
?>

<h1>Create TbFoto</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>