<?php

/**
 * This is the model class for table "tb_user".
 *
 * The followings are the available columns in table 'tb_user':
 * @property string $id
 * @property string $user
 * @property string $password
 * @property string $namadepan
 * @property string $namabelakang
 * @property string $namaperusahaan
 * @property string $alamat
 * @property string $kota
 * @property string $provensi
 * @property string $kodepos
 * @property string $email
 * @property string $telephon
 * @property integer $level_id
 * @property string $kode
 * @property string $status
 */
class TbUser extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_user';
	}

	//variabel-variabel
	public $validation;
	
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user, password, namadepan, email, level_id, kode, status', 'required'),
			array('level_id', 'numerical', 'integerOnly'=>true),
			array('user, password, namadepan, namabelakang, namaperusahaan, kota, provensi, email, kode', 'length', 'max'=>255),
			array('kodepos', 'length', 'max'=>100),
			array('telephon', 'length', 'max'=>20),
			array('status', 'length', 'max'=>5),
			array('alamat', 'safe'),
			array(
                     'validation',
                     'application.extensions.recaptcha.EReCaptchaValidator',
                     'privateKey'=> '6Ld54AkAAAAAAL4pjUhDHjzmpn_xN4ivtxuodPvL', 
                     'on' => 'registerwcaptcha'
                 ),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user, password, namadepan, namabelakang, namaperusahaan, alamat, kota, provensi, kodepos, email, telephon, level_id, kode, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user' => 'User',
			'password' => 'Password',
			'namadepan' => 'Nama depan',
			'namabelakang' => 'Nama belakang',
			'namaperusahaan' => 'Namaperusahaan',
			'alamat' => 'Alamat',
			'kota' => 'Kota',
			'provensi' => 'Provensi',
			'kodepos' => 'Kodepos',
			'email' => 'Email',
			'telephon' => 'Telephon',
			'level_id' => 'Level',
			'kode' => 'Kode',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('user',$this->user,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('namadepan',$this->namadepan,true);
		$criteria->compare('namabelakang',$this->namabelakang,true);
		$criteria->compare('namaperusahaan',$this->namaperusahaan,true);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('kota',$this->kota,true);
		$criteria->compare('provensi',$this->provensi,true);
		$criteria->compare('kodepos',$this->kodepos,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('telephon',$this->telephon,true);
		$criteria->compare('level_id',$this->level_id);
		$criteria->compare('kode',$this->kode,true);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TbUser the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function validatePassword($password) 
    { 
        return $this->hashPassword($password,$this->kode)===$this->password; 
    } 
     
    public function hashPassword($password,$salt) 
    { 
        return md5($salt.$password); 
    } 
 
	 public function generateSalt() 
    { 
        return uniqid('',true); 
	}
}
