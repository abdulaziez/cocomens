<?php

/**
 * This is the model class for table "tb_keranjang".
 *
 * The followings are the available columns in table 'tb_keranjang':
 * 
 * @property string $ids
 * @property sring $no_order
 * @property string $idbarang
 * @property string $idukuran
 * @property integer $jumlahpembelian
 * @property integer $jumlahharga
 */
class TbKeranjang extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_keranjang';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ids, idbarang, idukuran, jumlahpembelian, jumlahharga', 'required'),
			array('jumlahpembelian,  jumlahharga', 'numerical', 'integerOnly'=>true),
			array('no_order, ids, idbarang, idukuran', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('no_order, idbarang, idukuran, jumlahpembelian, jumlahharga', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ids' => 'Ids',
                        'no_order' => 'No Order',
			'idbarang' => 'Idbarang',
			'idukuran' => 'Ukuran',
			'jumlahpembelian' => 'Qty',
			'jumlahharga' => 'Jumlahharga',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ids',$this->ids,true);
                $criteria->compare('no_order',$this->no_order,true);
		$criteria->compare('idbarang',$this->idbarang,true);
		$criteria->compare('idukuran',$this->idukuran,true);
		$criteria->compare('jumlahpembelian',$this->jumlahpembelian);
		$criteria->compare('jumlahharga',$this->jumlahharga);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TbKeranjang the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        
        public function listKeranjang($idses)
        {
            $sql='SELECT * FROM tb_keranjang WHERE ids = "'.$idses.'" order by ids desc';
                $dataProvider=new CSqlDataProvider($sql,array(
                'keyField' => 'ids',
                ));
                return $dataProvider;
            
        }
}
