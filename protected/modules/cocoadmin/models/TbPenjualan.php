<?php

/**
 * This is the model class for table "tb_penjualan".
 *
 * The followings are the available columns in table 'tb_penjualan':
 * @property string $id
 * @property string $iduser
 * @property string $no_order
 * @property integer $biayakirim
 * @property integer $total_harga
 * @property integer $diskon
 * @property string $status_pembayaran
 * @property string $status_pengiriman
 * @property string $tgl_pesan
 * @property string $jam_pesan
 * @property string $tgl_pengiriman
 * @property string $kode_pembelian
 * @property string $verifikasi_email
 */
class TbPenjualan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_penjualan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('iduser, no_order, biayakirim, total_harga, diskon, status_pembayaran, status_pengiriman, tgl_pesan, jam_pesan, tgl_pengiriman, kode_pembelian, verifikasi_email', 'required'),
			array('biayakirim, total_harga, diskon', 'numerical', 'integerOnly'=>true),
			array('iduser', 'length', 'max'=>20),
			array('no_order, verifikasi_email', 'length', 'max'=>5),
			array('status_pembayaran', 'length', 'max'=>13),
			array('status_pengiriman', 'length', 'max'=>8),
			array('kode_pembelian', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, iduser, no_order, biayakirim, total_harga, diskon, status_pembayaran, status_pengiriman, tgl_pesan, jam_pesan, tgl_pengiriman, kode_pembelian, verifikasi_email', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'iduser' => 'Iduser',
			'no_order' => 'No Order',
			'biayakirim' => 'Biayakirim',
			'total_harga' => 'Total Harga',
			'diskon' => 'Diskon',
			'status_pembayaran' => 'Status Pembayaran',
			'status_pengiriman' => 'Status Pengiriman',
			'tgl_pesan' => 'Tgl Pesan',
			'jam_pesan' => 'Jam Pesan',
			'tgl_pengiriman' => 'Tgl Pengiriman',
			'kode_pembelian' => 'Kode Pembelian',
			'verifikasi_email' => 'Verifikasi Email',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('iduser',$this->iduser,true);
		$criteria->compare('no_order',$this->no_order,true);
		$criteria->compare('biayakirim',$this->biayakirim);
		$criteria->compare('total_harga',$this->total_harga);
		$criteria->compare('diskon',$this->diskon);
		$criteria->compare('status_pembayaran',$this->status_pembayaran,true);
		$criteria->compare('status_pengiriman',$this->status_pengiriman,true);
		$criteria->compare('tgl_pesan',$this->tgl_pesan,true);
		$criteria->compare('jam_pesan',$this->jam_pesan,true);
		$criteria->compare('tgl_pengiriman',$this->tgl_pengiriman,true);
		$criteria->compare('kode_pembelian',$this->kode_pembelian,true);
		$criteria->compare('verifikasi_email',$this->verifikasi_email,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TbPenjualan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
