<?php

/**
 * This is the model class for table "tb_op".
 *
 * The followings are the available columns in table 'tb_op':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property integer $level_id
 * @property string $has
 */
class TbOp extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_op';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username, password, level_id', 'required'),
			array('level_id', 'numerical', 'integerOnly'=>true),
			array('username, password, has', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, username, password, level_id, has', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'username' => 'Username',
			'password' => 'Password',
			'level_id' => 'Level',
			'has' => 'Has',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('level_id',$this->level_id);
		$criteria->compare('has',$this->has,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TbOp the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
          public function validatePassword($password) 
        { 
            return $this->hashPassword($password,$this->has)===$this->password; 
        } 

        public function hashPassword($password,$salt) 
        { 
            return md5($salt.$password); 
        } 

             public function generateSalt() 
        { 
            return uniqid('',true); 
        }
}
